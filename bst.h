struct Node
{
    int data;
    struct Node *left;
    struct Node *right;
};


class bst
{	struct Node* root;
	public:
		bst();
		struct Node* newNode(int val);
		bool hasPathSum(struct Node* node,int sum);
		int count(struct Node* node);
		struct Node* getRoot();
		void setRoot(struct Node* rt);
		struct Node* insert(struct Node* tree, int val);
		void mirror(struct Node* node);
		void printTree(struct Node* tree);
		void printPostorder(struct Node* tree);
		int maxDepth(struct Node* node);
		int size(struct Node* node);
		int lookup(int val);
		int minValue(struct Node* node);
        void doubleTree(struct Node* node);
        void printPaths(struct Node* node);
        void printPathsRecur(struct Node* node, int path[], int pathLen);
        int sameTree(struct Node* a, struct Node* b);
        int countTrees(int tNodes);
};
