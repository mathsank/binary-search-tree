#include<iostream>
#include "bst.h"

using namespace std;
		     bst:: bst()
			{ root = NULL; }
	struct Node* bst:: getRoot()
	{	return root;
	}
	void bst:: setRoot(struct Node* rt)
	{ root = rt;
	}
	struct Node* bst:: newNode(int item)
	{
    		struct Node *temp =  (struct Node *)malloc(sizeof(struct Node));
    		temp->data = item;
    		temp->left = temp->right = NULL;
    		return temp;
	}	

	struct Node* bst:: insert(struct Node* tree, int val)	// inserts node with value at its position
	{
		 if (tree == NULL)				
   		 {	
        		return newNode(val);
   		 }
		
		if (val> tree->data)
			tree->right = insert(tree->right, val);
			
		else if(val< tree->data)
			tree->left = insert(tree->left, val);
		
	 	
	
	return tree;
	}

	void bst:: printTree(struct Node* node)
	{	if (node == NULL)
    		{
        		//cout<<"Tree is empty"<<endl;
        		return;
   		}
    	
        		printTree(node->left);
        		cout<<node->data<<"  ";
        		printTree(node->right);
   	}
	void bst:: printPostorder(struct Node* tree)
	{
		
    		if(tree==NULL)
   		 {
        		return;
   		 }
    			printPostorder(tree->left);
    			printPostorder(tree->right);
    			cout<<tree->data<<" ";
	}

	int bst :: count(Node *node)
	{
    		int c = 1;
 
   		if (node == NULL)			//returns 0 if root is 0 or if leaf node is reached so 0 will be added onto sum
        	  return 0;
    		else
    		{
        	  c += count(node->left);		//counts left side of bst
        	  c += count(node->right);		// counts right side of bst
        	  return c;
   		}
	}

	int bst:: maxDepth(struct Node* node) 
	{
		if (node==NULL) 
       		  return 0;				// returns 0 if root is null or if leaf node is reached
   		else
   		{
       		  int lDepth = maxDepth(node->left);		//depth of left subtree recursively
		  int rDepth = maxDepth(node->right);		// depth of right subtree recursively
 
       
       		if (lDepth > rDepth) 				//find larger one
           		return(lDepth+1);
       			else return(rDepth+1);
   		}
	} 
	int bst:: minValue(struct Node* node) 
	{
		struct Node* current = node;

		while (current->left != NULL)			// min most will be left most so iterate till leaf node is reached
		{
    		  current = current->left;
 		}
  		return(current->data);
	} 

	bool bst:: hasPathSum(struct Node* node, int sum)
	{
  		// return true if leaf node reached and sum==0
  		if (node == NULL)
		{
    		 return(sum == 0);
 		}
  		else 
		{
  			bool ans = 0;  
  
    			// otherwise check left and right subtrees 
    			int subSum = sum - node->data;
  
   
   		 if ( subSum == 0 && node->left == NULL && node->right == NULL )		// if leaf node reached and sum is 0 then return true
      			return 1;
  
    		 if(node->left)
      			ans = ans || hasPathSum(node->left, subSum);
    		 if(node->right)
      			ans = ans || hasPathSum(node->right, subSum);
  
    		 return ans;  

		}
	}

	void bst:: mirror(struct Node* node)
	{
  		if (node==NULL)
		{
    		return;
		}
		else
		{
    			struct Node* temp;
    			// do the same for left and right subtrees
    			mirror(node->left);
    			mirror(node->right);

    			// swap the pointers of current node
    			temp = node->left;
    			node->left = node->right;
    			node->right = temp;
		}
 	}
	void bst:: doubleTree(struct Node* node)
	{       struct Node* oldL;

       		if (node==NULL) return;

        	//recursively do for left and right subtrees
        	doubleTree(node->left);
       		doubleTree(node->right);

        	// duplicate current node to its left
        	oldL = node->left;
        	node->left = newNode(node->data);
        	node->left->left = oldL;
	}
	void bst:: printPaths(struct Node* node)
	{
        	int path[1000];
        	printPathsRecur(node, path, 0);
   	}

    	void bst::printPathsRecur(struct Node* node, int path[], int pathLen)
   	{
        	if (node==NULL)             // no nodes in tree
            	return;
        
       		// add current node to the path array
        	path[pathLen] = node->data;
        	pathLen++;
        
        	if (node->left==NULL && node->right==NULL)      //leaf node reached so print path to current position
        	{
            	for(int i=0; i<pathLen; i++)
            	{
                	cout<<path[i];
            	}
            	cout<<endl;
        	}
        	else
        	{
            	//recursively do for left and right subtrees
            	printPathsRecur(node->left, path, pathLen);
            	printPathsRecur(node->right, path, pathLen);
        	}
    	}
    	int bst:: sameTree(struct Node* a, struct Node* b)
    	{
    
        	if (a==NULL && b==NULL)             //  both empty then true
            	return  true;
    
        	else if (a!=NULL && b!=NULL)        // both non-empty then compare them
        	{
            		bool x;
            		x = (a->data == b->data) && (sameTree(a->left, b->left)) && (sameTree(a->right, b->right));
            		return x;
       		 }
    
        	else 
		return false;                 // one empty one not empty then false
    	}
    int bst:: countTrees(int tNodes)
    {
        if (tNodes <=1)
        {
            return(1);
        }
        else
        {
        // each node can be root so loop from 1 to total number of nodes
            int sum = 0;
            int left, right, root;
            for (root=1; root<=tNodes; root++)
            {
                left = countTrees(root - 1);            //left and right nodes form subtrees
                right = countTrees(tNodes - root);
                // number of possible trees with for one root is left*right
                sum += left*right;
            }
        return(sum);
        }
    }

int main()
{
	int choice, num,val;
    	bst bst1, bst2;
    	Node *temp, *rt, *rt2;
	rt = bst1.getRoot();
    rt2 = bst2.getRoot();
	bool x;
	
    while (1)
    {
        cout<<"-----------------"<<endl;
        cout<<"Operations on BST"<<endl;
        cout<<"-----------------"<<endl;
        cout<<"1.Insert Element "<<endl;
        cout<<"2.Print post order "<<endl;
        cout<<"3.Inorder Traversal"<<endl;
        cout<<"4.Number of nodes"<<endl;
        cout<<"5.Maximum depth"<<endl;
        cout<<"6.Minimum most value"<<endl;
        cout<<"7.Has path sum"<<endl;
        cout<<"8.Mirror "<<endl;
        cout<<"9.Double Tree"<<endl;
        cout<<"10.Print paths"<<endl;
        cout<<"11. Same trees checking"<<endl;
        cout<<"12. Total unique bsts"<<endl;
        cout<<"13.Quit"<<endl;
        cout<<"Enter your choice : ";
        cin>>choice;
        switch(choice)
        {
        case 1:
            	cout<<"Enter the number to be inserted : ";
	    	cin>>val;
		cout<<"Enter tree number"<<endl;
		cin>>num;
		if(num==1)
		{ rt = bst1.insert(rt, val);
		  bst1.setRoot(rt);
		}
		else
		{ rt2 = bst2.insert(rt2,val);
		  bst2.setRoot(rt2);
		}
		break;
        case 2:
		rt = bst1.getRoot();
	    	bst1.printPostorder(rt);
            	break;
        
        case 3:
	    rt = bst1.getRoot();
	    rt2 = bst2.getRoot();
            cout<<"Inorder Traversal of BST:"<<endl;
            cout<<"Tree 1 : ";
	    bst1.printTree(rt);
	    cout<<endl;
	    cout<<"Tree 2 : ";
	    bst2.printTree(rt2);
            cout<<endl;
            break;
        case 4:
	    rt = bst1.getRoot();
                rt2 = bst2.getRoot();
            cout<<"No. of nodes in tree 1: "<<endl;
            num = bst1.count(rt);
            cout<<num<<endl;
                cout<<"No. of nodes in tree 2: "<<endl;
                num = bst2.count(rt2);
                cout<<num<<endl;
            break;
        case 5:
	    rt = bst1.getRoot();
                rt2 = bst2.getRoot();
            cout<<"Maximum depth of tree 1: "<<endl;
            num = bst1.maxDepth(rt);
            cout<<num<<endl;
                cout<<"Maximum depth of tree 2: "<<endl;
                num = bst2.maxDepth(rt2);
                cout<<num<<endl;
            break;
        case 6:
	    rt = bst1.getRoot();
            cout<<"Minimum value of tree 1:"<<endl;
            num = bst1.minValue(rt);
            cout<<num<<endl;
                rt2 = bst2.getRoot();
                cout<<"Minimum value of tree 2:"<<endl;
                num = bst2.minValue(rt2);
                cout<<num<<endl;
            break;
        case 7: 
	    rt=bst1.getRoot();
	    cout<<"Enter sum to check : ";
	    int sum;
	    cin>>sum;
        cout<<"Enter tree number : ";
        cin>>num;
        if(num==1)
	    x = bst1.hasPathSum(rt,sum);
	    else
        x = bst2.hasPathSum(rt2,sum);
        x==1?cout<<"Yes"<<endl:cout<<"No"<<endl;
	    break;
        case 8:
        rt2=bst2.getRoot();
        rt=bst1.getRoot();
        cout<<"Enter tree number : ";
        cin>>num;
		cout<<"Mirror is "<<endl;
		
        if(num==1)
        {   bst1.mirror(rt);
            rt = bst1.getRoot();
            bst1.printTree(rt);
        }
        else
        {   bst2.mirror(rt2);
            rt2 = bst2.getRoot();
            bst2.printTree(rt2);
        }
		
		break;
        case 9: cout<<"Enter tree number : ";
                cin>>num;
                if(num==1)
                {
                    rt=bst1.getRoot();
                    bst1.doubleTree(rt);
                    rt = bst1.getRoot();
                    bst1.printTree(rt);
                }
                else
                {
                    rt2=bst2.getRoot();
                    bst2.doubleTree(rt2);
                    rt2 = bst2.getRoot();
                    bst2.printTree(rt2);
                }
                break;
        case 10:
                cout<<"Enter tree number : ";
                cin>>num;
                if(num==1)
                {
                rt=bst1.getRoot();
                bst1.printPaths(rt);
                }
                else
                {
                    rt2=bst2.getRoot();
                    bst2.printPaths(rt2);
                }
                break;
        case 11:rt = bst1.getRoot();
                rt2 = bst2.getRoot();
                if(bst1.sameTree(rt,rt2))
                    cout<<"Same trees"<<endl;
                else
                    cout<<"Not same"<<endl;
                break;
            case 12: int len;
                cout<<"Enter tree number : ";
                cin>>num;
                
                if(num==1)
                {   rt = bst1.getRoot();
                    len = bst1.count(rt);
                    val = bst1.countTrees(len);
                    
                }
                else
                {
                    rt2 = bst2.getRoot();
                    len = bst2.count(rt2);
                    val = bst2.countTrees(len);
                }
		cout<<val<<endl;
                break;
        case 13:
            exit(1);
        default:
            cout<<"Wrong choice"<<endl;
        }
    }

return 0;
}
	
